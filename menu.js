var bmpText;
var chosen = 0;

var menu = {
    preload:preload,
    create:create,
    update:update
}
var title;
var text1;
var text2;
var text3;

var up;
var down;
var enter;
var esc;
function create(){
    bg = game.add.tileSprite(0,0,game.width,game.height,'bg');
    up = game.input.keyboard.addKey(Phaser.Keyboard.UP);
    down = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
    enter = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
    esc = game.input.keyboard.addKey(Phaser.Keyboard.ESC);
    title = game.add.text(game.world.centerX, game.world.centerY-100, "Space Raiden");
    text1 = game.add.text(game.world.centerX, game.world.centerY+50, "Play");
    text2 = game.add.text(game.world.centerX, game.world.centerY+100, "LeaderBoard");
    text3 = game.add.text(game.world.centerX, game.world.centerY+150, "Quit");
    music = game.add.audio('music');
    music.play();
}
function update(){
    createtext();
    colortext();
    bg.tilePosition.y -= 5;
    choose();
}
function createtext(){
    title.anchor.setTo(0.5);
    title.fontSize = 60;

    text1.anchor.setTo(0.5);
    text1.fontSize = 36;

    text2.anchor.setTo(0.5);
    text2.fontSize = 36;

    text3.anchor.setTo(0.5);
    text3.fontSize = 36;
}
function colortext(){
    title.fill = "#ffffff";
    if(chosen == 0){
        text1.fill = "#c6b93f";
        text2.fill = "#ffffff";
        text3.fill = "#ffffff";
    }
    else if(chosen == 1){
        text1.fill = "#ffffff";
        text2.fill = "#c6b93f";
        text3.fill = "#ffffff";
    }
    else if(chosen == 2){
        text1.fill = "#ffffff";
        text2.fill = "#ffffff";
        text3.fill = "#c6b93f";
    }
}
function choose(){
    if(up.isDown&&up.downDuration(1)){
        if(chosen==0){chosen = 2;}
        else {chosen -=1;}
    }
    else if(down.isDown&&down.downDuration(1)){
        if(chosen==2){chosen = 0;}
        else {chosen +=1;}
    }
    else if(enter.isDown){
        if(chosen==0){game.state.start('main');}
        else if(chosen==1){}
        else if(chosen==2){window.close();}
    }
}
